
## Site de vente en ligne

Juste un autre site avec le framework Django. Il faut bien tester !

### virtualenv

Cette fois, un environnement virtuel est utilisé.

Installation du projet

    git clone git@gitlab.com:aloha68/rhum-express.git
    cd rhum-express
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    

