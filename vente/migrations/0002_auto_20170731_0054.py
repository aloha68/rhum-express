# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-31 00:54
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vente', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LigneCommande',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantite', models.IntegerField(default=1)),
            ],
        ),
        migrations.RenameModel(
            old_name='Bouteille',
            new_name='Produit',
        ),
        migrations.AddField(
            model_name='commande',
            name='date_paiement',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='lignecommande',
            name='commande',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vente.Commande'),
        ),
        migrations.AddField(
            model_name='lignecommande',
            name='produit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vente.Produit'),
        ),
        migrations.AddField(
            model_name='commande',
            name='produits',
            field=models.ManyToManyField(through='vente.LigneCommande', to='vente.Produit'),
        ),
    ]
