from django.conf.urls import url

from .views import IndexView, PanierView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^panier$', PanierView.as_view(), name='panier'),
]
