import datetime
from django.db import models

class Produit(models.Model):
    nom = models.CharField(max_length=200)
    description = models.TextField()
    image = models.CharField(max_length=50)
    prix = models.DecimalField(default=0, max_digits=12, decimal_places=2)
    stock = models.IntegerField(default=0)

    def __str__(self):
        return "{} ({} €, stock: {})".format(self.nom, self.prix, self.stock)

class Commande(models.Model):
    email = models.EmailField()
    date_paiement = models.DateTimeField(default=datetime.datetime.now)
    produits = models.ManyToManyField(Produit, through='LigneCommande')

    def __str__(self):
        return "{} - {}".format(self.date_paiement, self.email)

class LigneCommande(models.Model):
    produit = models.ForeignKey(Produit)
    commande = models.ForeignKey(Commande, on_delete=models.CASCADE)
    quantite = models.IntegerField(default=1)

    def __str__(self):
        return "{} {} ({})".format(self.quantite, self.produit.nom, self.commande)
