from django.views.generic import TemplateView

from .models import Produit, Commande, LigneCommande

class VenteView(TemplateView):

    def __init__(self):
        super(TemplateView, self).__init__()

    def get_panier(self):
        """Récupère le panier de l'utilisateur courant"""
        
        panier = self.request.session.get('panier')
        if not panier:
            panier = []
            self.request.session.pop('panier', panier)

        return panier

class IndexView(VenteView):
    template_name = 'index.html'

    def __init__(self):
        super(VenteView, self).__init__()
    
    def get_context_data(self, **kwargs):
        """Call the base implementation and add some custom variables to it"""

        context = super(TemplateView, self).get_context_data(**kwargs)
        context['produits'] = Produit.objects.all()
        context['panier'] = self.get_panier()
        return context

class PanierView(VenteView):
    template_name = 'index.html'

    def __init__(self):
        super(VenteView, self).__init__()

    def get_context_data(self, **kwargs):
        """Call the base implementation and add some custom variables to it"""

        context = super(TemplateView, self).get_context_data(**kwargs)
        context['produits'] = []
        return context
